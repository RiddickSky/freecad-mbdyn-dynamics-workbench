# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################

import FreeCAD
import Draft

class Distance:
    def __init__(self, obj, label, node1, node2, referenceObject1, referenceObject2, reference1, reference2):        

        obj.addExtension("App::GroupExtensionPython")  
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","Distance joint","label",1).label = label
        obj.addProperty("App::PropertyString","joint","Distance joint","joint",1).joint = 'distance'
        obj.addProperty("App::PropertyString","node 1","Distance joint","node 1",1).node_1 = node1.label
        obj.addProperty("App::PropertyString","node 2","Distance joint","node 2",1).node_2 = node2.label
        obj.addProperty("App::PropertyString","distance","Distance joint","distance").distance = "node"
        obj.addProperty("App::PropertyString","modifier","Distance joint","modifier").modifier = "*1." 
        
        #The relative offset is initially set to (0,0,0), this is, node 2 is at the line, without offset
        obj.addProperty("App::PropertyDistance","relative offset 1_X","Line offset relative to node 2","relative offset 1_X",1).relative_offset_1_X = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        obj.addProperty("App::PropertyDistance","relative offset 1_Y","Line offset relative to node 2","relative offset 1_Y",1).relative_offset_1_y = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        obj.addProperty("App::PropertyDistance","relative offset 1_Z","Line offset relative to node 2","relative offset 1_Z",1).relative_offset_1_z = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))

        obj.addProperty("App::PropertyDistance","relative offset 2_X","Line offset relative to node 2","relative offset 2_X",1).relative_offset_2_X = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        obj.addProperty("App::PropertyDistance","relative offset 2_Y","Line offset relative to node 2","relative offset 2_Y",1).relative_offset_2_Y = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        obj.addProperty("App::PropertyDistance","relative offset 2_Z","Line offset relative to node 2","relative offset 2_Z",1).relative_offset_2_Z = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))        
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","Animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","Animation","frame")
        obj.frame=['global','local']  
        
        obj.addProperty("App::PropertyString","structural dummy","Animation","structural dummy").structural_dummy = '2'
        
        obj.addProperty("App::PropertyFloat","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = 1.0

        
        #The references define the position and orientation of the joint:                
        obj.addProperty("App::PropertyLinkSub","reference_1","Reference 1","reference_1")
        obj.addProperty("App::PropertyEnumeration","attachment_mode_1","Reference 1","attachment mode 1")
        obj.attachment_mode_1 = ['geometry´s center of mass', 'body´s center of mass', 'arc´s center','start point','end point']
        
        obj.addProperty("App::PropertyLinkSub","reference_2","Reference 2","reference_2")        
        obj.addProperty("App::PropertyEnumeration","attachment_mode_2","Reference 2","attachment mode 2")
        obj.attachment_mode_2 = ['geometry´s center of mass', 'body´s center of mass', 'arc´s center','start point','end point']      

        if referenceObject1 == referenceObject2:
            obj.reference_1 = (referenceObject1, reference1.SubElementNames[0])
            obj.reference_2 = (referenceObject2, reference2.SubElementNames[1])            
        
        else:
            obj.reference_1 = (referenceObject1, reference1.SubElementNames[0])
            obj.reference_2 = (referenceObject2, reference2.SubElementNames[0]) 

        obj.Proxy = self
        
        #Add the vector to visualize reaction forces           
        p1 = obj.reference_1[0].Shape.getElement(obj.reference_1[1][0]).CenterOfMass
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))                                          
        p2 = FreeCAD.Vector( p1.x+Llength.Value, p1.y+Llength.Value, p1.z+Llength.Value)  
        
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.Label = "jf: "+ label                                        
        
    def execute(self, fp):
        JF = FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+fp.label)[0]
        
        if fp.attachment_mode_1 == 'geometry´s center of mass':
            p1 = fp.reference_1[0].Shape.getElement(fp.reference_1[1][0]).CenterOfGravity
            
        if fp.attachment_mode_2 == 'geometry´s center of mass':
            p2 = fp.reference_2[0].Shape.getElement(fp.reference_2[1][0]).CenterOfGravity
        
        if fp.attachment_mode_1 == 'body´s center of mass':
            p1 = fp.reference_1[0].Shape.CenterOfGravity
            
        if fp.attachment_mode_2 == 'body´s center of mass':    
            p2 = fp.reference_2[0].Shape.CenterOfGravity
            
        if fp.attachment_mode_1 == 'arc´s center':
            p1 = fp.reference_1[0].Shape.getElement(fp.reference_1[1][0]).Curve.Center
            
        if fp.attachment_mode_2 == 'arc´s center':
            p2 = fp.reference_2[0].Shape.getElement(fp.reference_2[1][0]).Curve.Center
            
        if fp.attachment_mode_1 == 'start point':
            p1 = fp.reference_1[0].Shape.getElement(fp.reference_1[1][0]).toNurbs().Edges[0].Curve.StartPoint

        if fp.attachment_mode_2 == 'start point':
            p2 = fp.reference_2[0].Shape.getElement(fp.reference_2[1][0]).toNurbs().Edges[0].Curve.StartPoint            

        if fp.attachment_mode_1 == 'end point':
            p1 = fp.reference_1[0].Shape.getElement(fp.reference_1[1][0]).toNurbs().Edges[0].Curve.EndPoint

        if fp.attachment_mode_2 == 'end point':
            p2 = fp.reference_2[0].Shape.getElement(fp.reference_2[1][0]).toNurbs().Edges[0].Curve.EndPoint

        #Get the joint´s nodes:
        node1 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: " + fp.node_1)[0]
        node2 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: " + fp.node_2)[0]
        
        #update the relative offsets:
        
        fp.relative_offset_1_X = FreeCAD.Units.Quantity(node1.absolute_position_X.Value - p1[0], FreeCAD.Units.Unit('mm'))
        fp.relative_offset_1_Y = FreeCAD.Units.Quantity(node1.absolute_position_Y.Value - p1[1], FreeCAD.Units.Unit('mm'))
        fp.relative_offset_1_Z = FreeCAD.Units.Quantity(node1.absolute_position_Z.Value - p1[2], FreeCAD.Units.Unit('mm'))

        fp.relative_offset_2_X = FreeCAD.Units.Quantity(node2.absolute_position_X.Value - p2[0], FreeCAD.Units.Unit('mm'))
        fp.relative_offset_2_Y = FreeCAD.Units.Quantity(node2.absolute_position_Y.Value - p2[1], FreeCAD.Units.Unit('mm'))
        fp.relative_offset_2_Z = FreeCAD.Units.Quantity(node2.absolute_position_Z.Value - p2[2], FreeCAD.Units.Unit('mm'))

        #Move the force arrow:            
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        JF.Start =  FreeCAD.Vector(p1[0], p1[1], p1[2])
        JF.End = FreeCAD.Vector(p1[0]+Llength, p1[1]+Llength, p1[2]+Llength) 
        
        FreeCAD.Console.PrintMessage("DISTANCE JOINT: " +fp.label+" successful recomputation...\n")
                           
                                        